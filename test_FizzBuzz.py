import unittest

from FizzBuzz import *

class MyTestCase(unittest.TestCase):
        
    def test_isDevThree(self):
        self.assertTrue(isDevThree(3))
        self.assertFalse(isDevThree(100))


    def test_Fizz(self):
        fizzList = []
        for num in range(1,101): fizzList.append(num) if num % 3 == 0 else None
        self.assertEqual(Fizz(100),fizzList)

    def test_isDevFive(self):
        self.assertTrue(isDevFive(5))
        self.assertFalse(isDevFive(98))
                    
    def test_Buzz(self):
        buzzList = []
        for num in range(1,101): buzzList.append(num) if num % 5 == 0 else None
        self.assertEqual(Buzz(100), buzzList)
        
    def test_FizzBuzz(self):
        fizzBuzzList = []
        for num in range(1,101): fizzBuzzList.append(num) if num % 3 == 0\
                                                                and num % 5 == 0\
                                                                        else None
        self.assertEqual(FizzBuzz(100), fizzBuzzList)

    def test_Output(self):
        outputList = []
        for num in range(1,101):
            if num % 3 == 0 and num % 5 == 0:
                outputList.append("FizzBuzz")
                continue
            if num % 3 == 0:
                outputList.append("Fizz")
                continue
            if num % 5 == 0:
                outputList.append("Buzz")
                continue
            else:
                outputList.append(num)
                continue
        self.assertEqual(Output(100), outputList)
        
if __name__ == '__main__':
    unittest.main()
