"""
[summary]: This function is executing the exercise detailed on:
http://codingdojo.org/kata/FizzBuzz/ with the for loop, we are
checking numbers divisible by, 5, and 5, changing the numbers
divisible by 3 for the string 'fizz', numbers divisible by 5 for
'buzz', numbers divisible by  and 5 for 'Fizzbuzz' and then appending
those numbers into the number list.

Returns:
    [numbers]:[list]
Returns the variable numbers that contains the list of all numbers
"""
# Fizz
def isDevThree(i):
    return i % 3 == 0

fizzList = []
def Fizz(i):   
    for num in range(1,i+1):
        if isDevThree(num):
            fizzList.append(num)
    return fizzList

# Buzz
def isDevFive(i):
    return i % 5 == 0

buzzList = []
def Buzz(i):
    for num in range (1,i+1):
        if isDevFive(num):
            buzzList.append(num)
    return buzzList

# FizzBuzz
fizzBuzzList = []
def FizzBuzz(i):
    for num in range (1,i+1):
        if isDevThree(num) and isDevFive(num):
            fizzBuzzList.append(num)
    return fizzBuzzList

# Output
outputList = []
def Output(i):
    for num in range(1,i+1):
        if num in fizzBuzzList:
            outputList.append("FizzBuzz")
            continue
        if num in fizzList:
            outputList.append("Fizz")
            continue
        if num in buzzList:
            outputList.append("Buzz")
            continue
        else:
            outputList.append(num)
            continue
    return outputList
